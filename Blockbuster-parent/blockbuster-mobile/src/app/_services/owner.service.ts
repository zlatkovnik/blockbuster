import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { Owner } from '../_models/Owners/owner';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  constructor() { }


  owners: Owner[] = [
    {
      firstName: 'Nikola',
      lastName: 'Zlatkov',
      email: 'nikola.zlatkov@dualsoft.com',
      imageUrl: 'https://i.ibb.co/hYLZcxR/NIKOLASLIKA.jpg',
      instaLink: 'https://www.instagram.com/nizla98/',
      fbLink: 'https://www.facebook.com/Nikola.Zlatkov.Sean',
      inLink: 'https://www.instagram.com/nizla98/',
      school: 'Elekentronski fakultet',
      city: 'Nis',
      phone: '+381 65 93 998 00'


    },
    {
      firstName: 'Petar',
      lastName: 'Brajkovic',
      email: 'petar.brajkovic@dualsoft.com',
      imageUrl: 'https://i.ibb.co/WsF6H1C/petarB.jpg',
      instaLink: 'https://www.instagram.com/t_novkovic/',
      fbLink: 'https://www.facebook.com/petar.brajkovic.7',
      inLink: 'https://www.linkedin.com/in/teodora-novkovic-729654135/',
      school: 'Elekentronski fakultet',
      city: 'Nis',
      phone: '+381 63 40 646 8'
    },
    {
      firstName: 'David',
      lastName: 'Nikolic',
      email: 'david.nikolic@dualsoft.com',
      imageUrl: 'https://i.ibb.co/bPj2QJD/davislika.jpg',
      instaLink: 'https://www.instagram.com/t_novkovic/',
      fbLink: 'https://www.facebook.com/david.nikolic.5070',
      inLink: 'https://www.linkedin.com/in/teodora-novkovic-729654135/',
      school: 'Elekentronski fakultet',
      city: 'Nis',
      phone: '+381 60 71 061 98'
    },
    {
      firstName: 'Teodora',
      lastName: 'Novkovic',
      email: 'teodora.novkovic@dualsoft.com',
      imageUrl: 'https://i.ibb.co/QCGr5J5/TEODORA-JA.jpg',
      instaLink: 'https://www.instagram.com/t_novkovic/',
      fbLink: 'https://www.facebook.com/teodora.novkovic.7/',
      inLink: 'https://www.linkedin.com/in/teodora-novkovic-729654135/',
      school: 'Elekentronski fakultet',
      city: 'Nis',
      phone: '+381 64 27 415 14'
    },

  ];



  getAllOwners() {
    return [...this.owners];
  }

}
