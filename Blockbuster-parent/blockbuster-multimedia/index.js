const path = require("path");
const express = require("express");
// const cors = require("cors");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, __dirname + "/uploads/images"),
  filename: (req, file, cb) =>
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    ),
});
const upload = multer({ storage });

const app = express();
const PORT = 3000;

app.use(express.static("uploads/images/"));
// app.use(cors);

app.post("/upload", upload.single("photo"), (req, res) => {
  if (req.file) {
    res.json(req.file);
  } else throw "error";
});

app.listen(PORT, () => {
  console.log("Listening at " + PORT);
});
