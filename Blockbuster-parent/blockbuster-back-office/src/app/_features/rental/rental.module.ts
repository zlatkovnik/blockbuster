import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RentalRequestsComponent } from "./rental-requests/rental-requests.component";
import {
  MatButtonModule,
  MatSortModule,
  MatTableModule,
} from "@angular/material";
import { SharedModule } from "src/app/_shared/shared.module";
import { RentalsComponent } from "./rentals/rentals.component";

@NgModule({
  declarations: [RentalRequestsComponent, RentalsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatSortModule,
    SharedModule,
  ],
  exports: [RentalRequestsComponent, RentalsComponent],
})
export class RentalModule {}
