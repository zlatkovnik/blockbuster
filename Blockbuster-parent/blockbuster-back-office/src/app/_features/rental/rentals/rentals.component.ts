import { Component, OnInit, ViewChild } from "@angular/core";
import { MatSort, MatTableDataSource } from "@angular/material";
import { ToastrService } from "ngx-toastr";
import { take } from "rxjs/operators";
import { Rental } from "src/app/_models/film/rental.model";
import { RentalService } from "src/app/_services/film/rental.service";
import { FilterService } from "src/app/_services/shared/filter.service";

@Component({
  selector: "app-rentals",
  templateUrl: "./rentals.component.html",
  styleUrls: ["./rentals.component.css"],
})
export class RentalsComponent implements OnInit {
  dataSource: MatTableDataSource<Rental>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  rentals: Rental[];

  displayedColumns = [
    "rentalId",
    "title",
    "rentalDate",
    "returnDate",
    "controls",
  ];

  constructor(
    private rentalService: RentalService,
    private toastr: ToastrService,
    private filterService: FilterService
  ) {}

  ngOnInit() {
    this.filterService.getObservable().subscribe((filter) => {
      if (filter.type === "customer") {
        this.rentalService
          .getRentals(filter.filterValue)
          .pipe(take(1))
          .subscribe((res) => {
            this.rentals = res.data;
            this.dataSource = new MatTableDataSource(this.rentals);
          });
      }
    });
    this.dataSource = new MatTableDataSource(this.rentals);
    this.dataSource.sort = this.sort;
  }

  returnInventory(rental: Rental) {
    this.rentalService
      .returnRental(rental.rentalId)
      .pipe(take(1))
      .subscribe(
        (res) => {
          rental.returnDate = new Date();
          this.toastr.success(`${rental.title} returned successfully!`);
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }
}
