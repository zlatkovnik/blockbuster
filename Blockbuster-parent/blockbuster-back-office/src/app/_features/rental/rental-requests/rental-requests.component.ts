import { AfterViewInit, Component, Input, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material";
import { ToastrService } from "ngx-toastr";
import { take } from "rxjs/operators";
import { Auth } from "src/app/_models/auth.model";
import { RentalRequest } from "src/app/_models/film/rental-request.model";
import { RentalService } from "src/app/_services/film/rental.service";
import { NotificationService } from "src/app/_services/real-time/notification.service";
import { FilterService } from "src/app/_services/shared/filter.service";
import { PageNavigationService } from "src/app/_services/shared/page-navigation.service";
@Component({
  selector: "app-rental-requests",
  templateUrl: "./rental-requests.component.html",
  styleUrls: ["./rental-requests.component.css"],
})
export class RentalRequestsComponent implements OnInit, AfterViewInit {
  dataSource: MatTableDataSource<RentalRequest>;
  @Input() rentalRequests: RentalRequest[];
  currentStatus = "all";
  currentCustomer = "all";
  @Input() auth: Auth;
  displayedColumns = [
    "rentalRequestId",
    "title",
    "username",
    "inventoryCount",
    "status",
    "createDate",
    "controls",
  ];

  nav: { page: number; count: number } = { page: 1, count: 20 };

  constructor(
    private filterService: FilterService,
    private rentalService: RentalService,
    private toastr: ToastrService,
    private notificationService: NotificationService,
    private pageNavigation: PageNavigationService
  ) {}

  ngOnInit() {
    this.fetchRentalRequests(this.nav.page, this.nav.count);
    this.filterService.getObservable().subscribe((filter) => {
      if (filter.type === "customer") {
        this.currentCustomer = filter.filterValue;
        this.fetchRentalRequests(this.nav.page, this.nav.count);
      } else if (filter.type === "status") {
        this.currentStatus = filter.filterValue;
        this.fetchRentalRequests(this.nav.page, this.nav.count);
      }
    });
    this.notificationService.getRentalRequestObservable().subscribe((rr) => {
      if (this.currentStatus === "all" || this.currentStatus === rr.status) {
        const existing = this.rentalRequests.find(
          (r) => r.rentalRequestId === rr.rentalRequestId
        );
        if (!existing) {
          this.rentalRequests.unshift(rr);
          this.dataSource.data = this.rentalRequests;
        } else {
          existing.status = rr.status;
          if (rr.status === "accepted") {
            existing.inventoryCount--;
          }
        }
      }
    });

    this.pageNavigation.getObservable().subscribe((res) => {
      this.nav = res;
      this.fetchRentalRequests(res.page, res.count);
    });
  }

  fetchRentalRequests(page: number, count: number) {
    this.rentalService
      .getRentalRequests(this.currentStatus, this.currentCustomer, page, count)
      .pipe(take(1))
      .subscribe(
        (res) => {
          this.rentalRequests = res.data;
          this.dataSource = new MatTableDataSource<RentalRequest>(
            this.rentalRequests
          );
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  //, { static: true }) sort: MatSort;

  ngAfterViewInit() {
    //this.dataSource.sort = this.sort;
  }

  acceptRentalRequest(rentalRequest: RentalRequest) {
    this.rentalService
      .setStatus(rentalRequest.rentalRequestId, "accepted")
      .pipe(take(1))
      .subscribe(
        (res) => {
          this.rentalRequests.map((r) => {
            if (r.rentalRequestId === rentalRequest.rentalRequestId) {
              r.status = "accepted";
            }
          });
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  rejectRentalRequest(rentalRequest: RentalRequest) {
    this.rentalService
      .setStatus(rentalRequest.rentalRequestId, "rejected")
      .pipe(take(1))
      .subscribe(
        (res) => {
          this.rentalRequests.find(
            (r) => r.rentalRequestId === rentalRequest.rentalRequestId
          ).status = "rejected";
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }
}
