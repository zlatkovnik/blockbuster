import { NotificationService } from "./../../../_services/real-time/notification.service";
import { AuthService } from "./../../../_services/auth.service";
import { Auth } from "./../../../_models/auth.model";
import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
  items: { icon: string; text: string; path: string }[] = [
    { text: "Add Film", icon: "movie_filter", path: "/addFilm" },
    { text: "Films", icon: "movie", path: "/films" },
    { text: "Actors", icon: "person", path: "/actors" },
    { text: "Staff", icon: "engineering", path: "/staff" },
    { text: "Customers", icon: "hail", path: "/customers" },
    { text: "Stores", icon: "store", path: "/stores" },
    { text: "Inventory", icon: "inventory_2", path: "/inventory" },
    { text: "Rentals", path: "/rentals", icon: "album" },
    {
      text: "Rental Requests",
      path: "/rentals/requests",
      icon: "pending_actions",
    },
  ];

  auth: Auth = undefined;

  filmItems: { icon: string; text: string; path: string }[];

  constructor(
    private authService: AuthService,
    private router: Router,
    private notificationService: NotificationService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    const auth = localStorage.AUTH;

    if (auth) {
      this.auth = JSON.parse(auth);
    }
    this.authService.getAuthObservable().subscribe((auth) => {
      this.auth = auth;
    });

    this.notificationService.getRentalRequestObservable().subscribe((rr) => {
      if (rr.status === "pending") {
        this.toastr.success(`Rental request for ${rr.title}`);
      }
    });
  }

  async logout() {
    this.router.navigate(["home"]);
    localStorage.removeItem("AUTH");
    this.authService.publishAuth(null);
  }
}
