import { Staff } from "./../../../_models/staff/staff-for-list";
import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-staff-list",
  templateUrl: "./staff-list.component.html",
  styleUrls: ["./staff-list.component.css"],
})
export class StaffListComponent implements OnInit {
  @Input() staff: Staff[];
  columns = ["staffId", "firstName", "lastName", "email", "username"];

  constructor() {}

  ngOnInit() {}
}
