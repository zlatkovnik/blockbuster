import { StaffListComponent } from './staff-list/staff-list.component';
import { MatTableModule } from "@angular/material";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    StaffListComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
  ],
  exports: [
    StaffListComponent
  ],
})
export class StaffModule { }
