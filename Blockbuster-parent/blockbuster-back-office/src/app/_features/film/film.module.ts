import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FilmListComponent } from "./film-list/film-list.component";
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatTableModule,
} from "@angular/material";
import { FilmEditDialogComponent } from "./film-edit-dialog/film-edit-dialog.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FilmAddFormComponent } from "./film-add-form/film-add-form.component";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { SharedModule } from "src/app/_shared/shared.module";
import { FilmInventoryDialogComponent } from "./film-inventory-dialog/film-inventory-dialog.component";

@NgModule({
  declarations: [
    FilmListComponent,
    FilmEditDialogComponent,
    FilmAddFormComponent,
    FilmInventoryDialogComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    ScrollingModule,
    SharedModule,
  ],
  exports: [FilmListComponent, FilmAddFormComponent],
  entryComponents: [FilmEditDialogComponent, FilmInventoryDialogComponent],
})
export class FilmModule {}
