import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { ToastrService } from "ngx-toastr";
import { take } from "rxjs/operators";
import { Auth } from "src/app/_models/auth.model";
import { FilmForEdit } from "src/app/_models/film/film-for-edit.model";
import { FilmInventoryForStore } from "src/app/_models/store/film-inventory-for-store.model";
import { FilmService } from "src/app/_services/film/film.service";
import { InventoryService } from "src/app/_services/film/inventory.service";
import { StoresService } from "src/app/_services/stores.service";

@Component({
  selector: "app-film-inventory-dialog",
  templateUrl: "./film-inventory-dialog.component.html",
  styleUrls: ["./film-inventory-dialog.component.css"],
})
export class FilmInventoryDialogComponent implements OnInit {
  stores: FilmInventoryForStore[];
  auth: Auth;

  constructor(
    public dialogRef: MatDialogRef<FilmInventoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { filmId: number },
    private inventoryService: InventoryService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    const auth = localStorage.AUTH;
    if (auth) {
      this.auth = JSON.parse(auth);
    }
    this.inventoryService
      .getFilmInventoryPerStore(this.data.filmId)
      .pipe(take(1))
      .subscribe(
        (res) => {
          return (this.stores = res.data.sort((a, b) => {
            if (a.storeId === this.auth.storeId) return -1;
            return 0;
          }));
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  addInventory(store: FilmInventoryForStore) {
    this.inventoryService
      .addInventory(this.data.filmId, store.storeId)
      .subscribe(
        (res) => {
          this.stores.map((s) => {
            if (s.storeId === store.storeId) {
              s.inventoryCount++;
              this.toastr.success(
                `Successfully added inventory to ${store.address}`
              );
            }
            return s;
          });
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  isMyStore(storeId: number) {
    if (storeId === this.auth.storeId) {
      return true;
    }
  }
}
