import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  MatDialogRef,
  MatSelectChange,
  MAT_DIALOG_DATA,
} from "@angular/material";
import { ToastrService } from "ngx-toastr";
import { Observable, pipe, zip } from "rxjs";
import { take } from "rxjs/operators";
import { Category } from "src/app/_models/film/category.model";
import { FilmForEdit } from "src/app/_models/film/film-for-edit.model";
import { FilmForList } from "src/app/_models/film/film-for-list.model";
import { Language } from "src/app/_models/film/language.model";
import { Rating } from "src/app/_models/film/rating.model";
import { CategoryService } from "src/app/_services/film/category.service";
import { FilmService } from "src/app/_services/film/film.service";
import { LanguageService } from "src/app/_services/film/language.service";

@Component({
  selector: "app-film-edit-dialog",
  templateUrl: "./film-edit-dialog.component.html",
  styleUrls: ["./film-edit-dialog.component.css"],
})
export class FilmEditDialogComponent implements OnInit {
  inputs: { formControlName: string; label: string }[] = [
    { formControlName: "title", label: "Title" },
    { formControlName: "releaseYear", label: "Release Year" },
    { formControlName: "length", label: "Length" },
    { formControlName: "starRating", label: "Star Rating" },
    { formControlName: "posterUrl", label: "Poster Url" },
    { formControlName: "rentalDuration", label: "Rental Duration" },
    { formControlName: "rentalRate", label: "Rental Rate" },
    { formControlName: "replacementCost", label: "Replacement Cost" },
  ];
  categories: Category[] = [];
  languages: Language[] = [];
  ratings = [Rating.G, Rating.NC_17, Rating.PG, Rating.PG_13, Rating.R];

  film: FilmForEdit;
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<FilmEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { filmId: number },
    private filmService: FilmService,
    private categoryService: CategoryService,
    private languageService: LanguageService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    zip(
      this.filmService.getFilmForEdit(this.data.filmId),
      this.categoryService.getAllCategories(),
      this.languageService.getAllLanguages()
    )
      .pipe(take(1))
      .subscribe(
        ([filmRes, categoriesRes, languagesRes]) => {
          this.handleFilms(filmRes.data);
          this.categories = categoriesRes.data;
          this.languages = languagesRes.data;
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  private handleFilms(film: FilmForEdit) {
    this.film = film;
    this.film.rating = this.film.rating.replace("_", "-");
    this.form = new FormGroup({
      title: new FormControl(this.film.title, [Validators.required]),
      length: new FormControl(this.film.length, [Validators.required]),
      starRating: new FormControl(this.film.starRating, [Validators.required]),
      posterUrl: new FormControl(this.film.posterUrl, [Validators.required]),
      description: new FormControl(this.film.description, [
        Validators.required,
      ]),
      rentalDuration: new FormControl(this.film.rentalDuration, [
        Validators.required,
      ]),
      rentalRate: new FormControl(this.film.rentalRate, [Validators.required]),
      replacementCost: new FormControl(this.film.replacementCost, [
        Validators.required,
      ]),
      releaseYear: new FormControl(this.film.releaseYear, [
        Validators.required,
      ]),
      rating: new FormControl(this.film.rating, [Validators.required]),
      languageId: new FormControl(this.film.languageId, [Validators.required]),
      categoryId: new FormControl(this.film.categoryId, [Validators.required]),
      availableInventory: new FormControl(this.film.availableInventory),
    });
  }

  close() {
    this.dialogRef.close();
  }

  selectionChange(type: string, change: MatSelectChange) {
    this.form.get(type).setValue(change.value);
  }

  addInventory() {
    this.film.totalInventory++;
    this.film.availableInventory++;
    this.form.get("availableInventory").setValue(this.film.availableInventory);
  }

  onSubmit() {
    if (this.form.valid) {
      const film: FilmForEdit = {
        ...this.form.value,
        filmId: this.film.filmId,
      };
      this.filmService
        .editFilm(film)
        .pipe(take(1))
        .subscribe(
          (res) => {
            const f: FilmForList = {
              ...res.data,
              categoryName: this.categories.find(
                (c) => c.categoryId === this.form.get("categoryId").value
              ).name,
              languageName: this.languages.find(
                (l) => l.languageId === this.form.get("languageId").value
              ).name,
            };
            this.toastr.success(`Successfully edited '${film.title}'`);
            this.dialogRef.close(f);
          },
          (err) => {
            if (err.error.message) {
              this.toastr.error(err.error.message);
            } else {
              this.toastr.error("Server not responding");
            }
          }
        );
    }
  }
}
