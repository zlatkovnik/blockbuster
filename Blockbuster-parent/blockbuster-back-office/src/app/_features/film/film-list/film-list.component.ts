import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { FilmForEdit } from "src/app/_models/film/film-for-edit.model";
import { FilmForList } from "src/app/_models/film/film-for-list.model";
import { FilmsForList } from "src/app/_models/film/films-for-list.model";
import { FilmEditDialogComponent } from "../film-edit-dialog/film-edit-dialog.component";
import { FilmInventoryDialogComponent } from "../film-inventory-dialog/film-inventory-dialog.component";

@Component({
  selector: "app-film-list",
  templateUrl: "./film-list.component.html",
  styleUrls: ["./film-list.component.css"],
})
export class FilmListComponent implements OnInit {
  @Input() films: FilmForList[];
  columns = [
    "id",
    "title",
    "releaseYear",
    "categoryName",
    "length",
    "languageName",
    "starRating",
    "rating",
    "edit",
  ];
  constructor(public dialogController: MatDialog) {}

  ngOnInit() {}

  openEditDialog(filmId: number) {
    const dialogRef = this.dialogController.open(FilmEditDialogComponent, {
      data: { filmId },
    });

    dialogRef.afterClosed().subscribe((res: FilmForEdit) => {
      if (!res) return;
      this.films = this.films.map((film) => {
        if (film.filmId !== res.filmId) return film;
        else return res as any;
      });
    });
  }

  openInventoryDialog(filmId: number) {
    const dialogRef = this.dialogController.open(FilmInventoryDialogComponent, {
      data: { filmId },
    });
  }
}
