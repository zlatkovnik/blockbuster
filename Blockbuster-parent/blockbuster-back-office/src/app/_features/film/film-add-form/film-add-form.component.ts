import { CategoryService } from "src/app/_services/film/category.service";
import { LanguageService } from "./../../../_services/film/language.service";
import { async } from "@angular/core/testing";
import { AfterViewInit, Component, ElementRef, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { FilmService } from "src/app/_services/film/film.service";
import { FilmForAdd } from "src/app/_models/film/film-for-add.model";
import { ViewChild } from "@angular/core";
import { debounceTime, distinctUntilChanged, take, tap } from "rxjs/operators";
import { fromEvent, zip } from "rxjs";
import { Category } from "src/app/_models/film/category.model";
import { Language } from "src/app/_models/film/language.model";
import { MatSelectChange } from "@angular/material";
import { ToastrService } from "ngx-toastr";
import { Actor } from "src/app/_models/actor/actor";
import { ActorService } from "src/app/_services/actor.service";
import { ActorMoviedb } from "src/app/_models/actor/actor-moviedb.model";

@Component({
  selector: "app-film-add-form",
  templateUrl: "./film-add-form.component.html",
  styleUrls: ["./film-add-form.component.css"],
})
export class FilmAddFormComponent implements OnInit, AfterViewInit {
  error: string = undefined;
  categories: Category[] = [];
  languages: Language[] = [];

  startCategory: number;
  startLanguage: number;

  @ViewChild("title", { static: false }) title: ElementRef;
  films: any = [];

  filmForAdd: FilmForAdd = new FilmForAdd();

  film: FormGroup = this.fb.group({
    title: ["", Validators.required],
    description: ["", Validators.required],
    releaseYear: ["", Validators.required],
    rentalDuration: ["", Validators.required],
    rentalRate: ["", Validators.required],
    length: ["", Validators.required],
    replacementCost: ["", Validators.required],
    starRating: ["", Validators.required],
    posterUrl: ["", Validators.required],
    categoryId: ["", Validators.required],
    languageId: ["", Validators.required],
    moviedbId: [0],
  });

  actors: ActorMoviedb[] = [];

  constructor(
    private fb: FormBuilder,
    private filmService: FilmService,
    private actorService: ActorService,
    private categoryService: CategoryService,
    private languageService: LanguageService,
    private toastr: ToastrService
  ) {}

  ngAfterViewInit() {
    fromEvent(this.title.nativeElement, "keydown")
      .pipe(
        debounceTime(150),
        // distinctUntilChanged(),
        tap((text) => {
          if (this.title.nativeElement.value == "") {
            this.films = [];
          } else {
            this.onTitleInput(this.title.nativeElement.value);
          }
        })
      )
      .subscribe();
  }

  ngOnInit() {
    zip(
      this.categoryService.getAllCategories(),
      this.languageService.getAllLanguages()
    )
      .pipe(take(1))
      .subscribe(
        ([categoriesRes, languagesRes]) => {
          this.categories = categoriesRes.data;
          this.languages = languagesRes.data;
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  onSubmit() {
    this.filmForAdd = this.film.value;
    this.filmService
      .addFilm(this.filmForAdd)
      .pipe(take(1))
      .subscribe(
        (res) => {
          this.actorService
            .addActorsAndRoles(this.filmForAdd.moviedbId)
            .pipe(take(1))
            .subscribe((res) => {
              this.toastr.success(
                `Successfully added ${this.filmForAdd.title}`
              );
              this.film.reset();
            });
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  selectionChange(type: string, change: MatSelectChange) {
    this.film.get(type).setValue(change.value);
  }

  onTitleInput(text: string) {
    this.filmService.getFilmSuggestion(text).subscribe(
      (res) => {
        this.films = res.data;
      },
      (err) => {
        if (err.error.message) {
          this.toastr.error(err.error.message);
        } else {
          this.toastr.error("Server not responding");
        }
      }
    );
  }

  handleClick(film: FilmForAdd) {
    this.startCategory = film.categoryId;
    this.startLanguage = film.languageId;

    this.film.get("categoryId").setValue(film.categoryId);
    this.film.get("languageId").setValue(film.languageId);

    this.film.get("title").setValue(film.title);
    this.film.get("description").setValue(film.description);
    this.film.get("releaseYear").setValue(film.releaseYear);
    this.film.get("length").setValue(film.length);
    this.film.get("starRating").setValue(film.starRating);
    this.film.get("posterUrl").setValue(film.posterUrl);
    this.film.get("moviedbId").setValue(film.moviedbId);

    this.actorService
      .getActorsFromMovieDB(film.moviedbId)
      .pipe(take(1))
      .subscribe(({ cast }) => {
        this.actors = cast;
      });

    this.films = [];
  }
}
