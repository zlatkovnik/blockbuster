import { MatDialogRef } from '@angular/material';
import { StaffService } from './../../../_services/staff.service';
import { AuthService } from './../../../_services/auth.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-stores-select-dialog',
  templateUrl: './stores-select-dialog.component.html',
  styleUrls: ['./stores-select-dialog.component.css']
})
export class StoresSelectDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<StoresSelectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private authService: AuthService,
    private staffService: StaffService,
    ) { }

  ngOnInit() {}

  selectStore() {
    this.authService.changeStoreId(this.data.storeId);
    this.staffService.updateStaff(this.data.staffId, this.data.storeId).subscribe()
  }

}
