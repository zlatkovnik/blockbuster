import { City } from './../../../_models/address/city';
import { Country } from './../../../_models/address/country';
import { take } from 'rxjs/operators';
import { StoreForEdit } from './../../../_models/store/store-for-edit';
import { StoresService } from 'src/app/_services/stores.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MatDialogRef } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-stores-edit-dialog',
  templateUrl: './stores-edit-dialog.component.html',
  styleUrls: ['./stores-edit-dialog.component.css']
})
export class StoresEditDialogComponent implements OnInit {

  public store: StoreForEdit;
  public countries: Country[] = [];
  public cities: City[] = [];

  public city: string;
  public country: string;

  form = new FormGroup({
    address: new FormControl("", [Validators.required]),
    postalCode: new FormControl("", [Validators.required]),
    cityId: new FormControl("", [Validators.required]),
    countryId: new FormControl("", [Validators.required]),
    latitude: new FormControl("", [Validators.required]),
    longitude: new FormControl("", [Validators.required]),
    district: new FormControl("", [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<StoresEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private storeService: StoresService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {

    this.store = { ...this.data };

    this.storeService.getCountries()
      .pipe(take(1))
      .subscribe((res) => {
        this.countries = res;
        this.handleForm()
      })

    this.storeService.getCityForCountry(this.store.countryId)
      .pipe(take(1))
      .subscribe((res) => {
        if (res != []) {
          this.city = res[res.findIndex(c => c.cityId === this.data.cityId)].city;
        }
        this.cities = res;
      },(err) => {
        this.dialogRef.close();
        if (err.error.message) {
          this.toastr.error(err.error.message);
        } else {
          this.toastr.error("Server not responding");
        }
      })

    this.country = this.data.country;
    this.city = this.data.city;
  }

  private handleForm() {

    this.form = new FormGroup({
      address: new FormControl(this.store.address, [Validators.required]),
      postalCode: new FormControl(this.store.postalCode, [Validators.required]),
      cityId: new FormControl(this.store.cityId, [Validators.required]),
      countryId: new FormControl(this.store.countryId, [Validators.required]),
      latitude: new FormControl(this.store.latitude, [Validators.required]),
      longitude: new FormControl(this.store.longitude, [Validators.required]),
      district: new FormControl(this.store.district, [Validators.required]),
    });
  }

  selectionChange(type: string, change) {
    this.form.get(type).setValue(change.value);

    this.form.get("longitude").setValue(null);
    this.form.get("latitude").setValue(null);
    this.form.get("address").setValue(null);
    this.form.get("postalCode").setValue(null);
    this.form.get("district").setValue(null);

    if (type == "countryId") {

      //this.store.countryId = change.value;
      // this.store.country = this.countries[this.countries.findIndex(c => c.countryId === change.value)].country;
      this.country = this.countries[this.countries.findIndex(c => c.countryId === change.value)].country;

      this.storeService.getCityForCountry(this.form.get("countryId").value)
        .pipe(take(1))
        .subscribe((res) => {
          if (res != []) {

            this.form.get("cityId").setValue(res[0].cityId);
            this.store.cityId = res[0].cityId;
            this.city = res[0].city;
          }
          this.cities = res;
        },(err) => {
          this.dialogRef.close();
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        });
    }
    else if (type == "cityId") {
      this.store.cityId = change.value;
      this.city = this.cities[this.cities.findIndex(c => c.cityId === change.value)].city;
    }
  }

  submit() {

    this.store.address = this.form.get("address").value;
    this.store.postalCode = this.form.get("postalCode").value;
    this.store.longitude = this.form.get("longitude").value;
    this.store.latitude = this.form.get("latitude").value;
    this.store.district = this.form.get("district").value;
    this.store.cityId = this.form.get("cityId").value;
    this.store.city = this.city;
    this.store.countryId = this.form.get("countryId").value;
    this.store.country = this.country;

    this.storeService.editStore(this.store)
      .subscribe((res) => {
        Object.assign(this.data, res);
        this.toastr.success("Store changed successfully");
      }, (err) => {
        this.dialogRef.close();
        if (err.error.message) {
          this.toastr.error(err.error.message);
        } else {
          this.toastr.error("Server not responding");
        }
      });
  }
}
