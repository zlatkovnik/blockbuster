import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSelectModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { StoresListComponent } from './stores-list/stores-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoresEditDialogComponent } from './stores-edit-dialog/stores-edit-dialog.component';
import { StoresSelectDialogComponent } from './stores-select-dialog/stores-select-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StoresAddDialogComponent } from './stores-add-dialog/stores-add-dialog.component';

@NgModule({
  declarations: [
    StoresListComponent,
    StoresEditDialogComponent,
    StoresSelectDialogComponent,
    StoresAddDialogComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatExpansionModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
  ],
  entryComponents: [
    StoresEditDialogComponent,
    StoresSelectDialogComponent,
    StoresAddDialogComponent
  ],
  exports: [
    StoresListComponent, 
    StoresEditDialogComponent,
    StoresSelectDialogComponent,
    StoresAddDialogComponent
  ]
})
export class StoresModule { }
