import { StoresSelectDialogComponent } from "./../stores-select-dialog/stores-select-dialog.component";
import { MatDialog } from "@angular/material";
import { StoresEditDialogComponent } from "./../stores-edit-dialog/stores-edit-dialog.component";
import { Component, Input, OnInit } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";

@Component({
  selector: "app-stores-list",
  templateUrl: "./stores-list.component.html",
  styleUrls: ["./stores-list.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("expanded", style({ height: "*", visibility: "visible" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class StoresListComponent implements OnInit {
  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty("detailRow");
  expandedElement: any;

  private _staffStoreId;
  private _staffId;
  private _stores;

  @Input()
  set stores(val) {
    this._stores = val;
  }
  get stores() {
    return this._stores;
  }

  @Input()
  set staffId(val) {
    this._staffId = val;
  }
  get staffId() {
    return this._staffId;
  }

  @Input()
  set staffStoreId(val) {
    this._staffStoreId = val;
  }
  get staffStoreId() {
    return this._staffStoreId;
  }

  columnsToDisplay = [
    "storeId",
    "address",
    "postalCode",
    "city",
    "country",
    "longitude",
    "latitude",
    "storeSelect",
    "editStore",
  ];

  columnsStaff = ["staffId", "firstName", "lastName", "username", "email"];

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  selectStore(storeId: number) {
    const dialogRef = this.dialog.open(StoresSelectDialogComponent, {
      data: { storeId: storeId, staffId: this.staffId },
    });
    this.expandedElement = null;
  }

  openEditDialog(store) {
    const dialogRef = this.dialog.open(StoresEditDialogComponent, {
      data: store,
    });
    this.expandedElement = null;
    dialogRef.afterClosed().subscribe((result) => {});
  }

  openRow(row) {
    if (this.expandedElement == row) {
      this.expandedElement = null;
    } else {
      this.expandedElement = row;
    }
  }
}
