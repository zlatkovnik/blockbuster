import { StoreForEdit } from './../../../_models/store/store-for-edit';
import { City } from './../../../_models/address/city';
import { Country } from './../../../_models/address/country';
import { take } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StoresService } from 'src/app/_services/stores.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-stores-add-dialog',
  templateUrl: './stores-add-dialog.component.html',
  styleUrls: ['./stores-add-dialog.component.css']
})
export class StoresAddDialogComponent implements OnInit {

  public store: Partial<StoreForEdit> = {}
  public countries: Country[] = [];
  public cities: City[] = [];
  public city: string;
  public country: string;
  public freeManagers;
  public selectedManager;

  form = new FormGroup({
    address: new FormControl('',[Validators.required]),
    postalCode: new FormControl('',[Validators.required]),
    cityId: new FormControl('',[Validators.required]),
    countryId: new FormControl('',[Validators.required]),
    latitude: new FormControl('',[Validators.required]),
    longitude: new FormControl('',[Validators.required]),
    district: new FormControl('',[Validators.required]),
    freeManagers: new FormControl('',[Validators.required]),
  })

  constructor(
    public dialogRef: MatDialogRef<StoresAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private storeService: StoresService,
    private toastr: ToastrService
    ) { }

  ngOnInit() {

    this.storeService.getFreeManagers().subscribe(res => {
      this.freeManagers = res
    });

    this.storeService.getCountries()
    .pipe(take(1))
    .subscribe((res) =>{
      this.countries = res;
      this.form.get("countryId").setValue(res[0].countryId);
    })

  }

  selectionChange(type: string, change) {
    this.form.get(type).setValue(change.value);

    if(type == "countryId"){
      
      this.country = this.countries[this.countries.findIndex(c => c.countryId === change.value)].country;

      this.storeService.getCityForCountry(this.form.get("countryId").value)
      .pipe(take(1))
      .subscribe((res) => {
        if(res != []){
          this.form.get("cityId").setValue(res[0].cityId);
          this.city = res[res.findIndex(c => c.cityId === res[0].cityId)].city;
        }
        this.cities = res;
    });
    }
    else if(type=="cityId") {
      this.city = this.cities[this.cities.findIndex(c => c.cityId === change.value)].city;
    }
  }

  Submit(){

    this.store.address = this.form.get("address").value;
    this.store.postalCode = this.form.get("postalCode").value;
    this.store.longitude = this.form.get("longitude").value;
    this.store.latitude = this.form.get("latitude").value;
    this.store.district = this.form.get("district").value;
    this.store.cityId = this.form.get("cityId").value;
    this.store.city = this.city;
    this.store.countryId = this.form.get("countryId").value;
    this.store.country = this.country;
    this.store.managerStaffId = this.form.get("freeManagers").value;

    this.storeService.addNewStore(this.store)
    .subscribe((res) => {
      this.toastr.success('Successfully added a new store',null,{positionClass:'toast-bottom-right'})
    },err => {
      this.toastr.error('Server error', null, {positionClass:'toast-top-right'})
      console.log(err);
    });
  }

}
