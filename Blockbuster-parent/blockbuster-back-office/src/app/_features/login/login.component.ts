import { AuthService } from './../../_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private toastr: ToastrService) { }

  ngOnInit() {
  }

  email = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*[@$!%*#?&_])[A-Za-z\d@$!%*#?&_]{8,}$/)]);

  hide = true;

  getErrorMessageForEmail(): string {
    if(this.email.hasError('noEmail')) {
      return 'The email or password are incorrect';
    }
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  getErrorMessageForPassword(): string {
    if(this.password.hasError('noPasswordForEmail')) {
      return 'The email or password are incorrect';
    }
    if (this.password.hasError('pattern')) {
      return 'Password must containt one capital letter and one special character'
    }
    return 'You must enter a value';
  }

  async login() {

    if(this.email.valid && this.password.valid) {
      const email = this.email.value;
      const password = this.password.value;
      this.authService
        .login(email, password)
        .pipe(
          catchError(err => {
            if(err.error.status === 422) {
              this.email.setErrors({noEmail: true})
            }
            else if (err.error.status === 403) {
              this.password.setErrors({noPasswordForEmail: true})
            }
            const emptyData = {data: null}
            return of(emptyData)
          })
        )
        .subscribe(async (res) => {
          if(res.data !== null){
            localStorage.setItem('AUTH', JSON.stringify(res.data));
            this.authService.publishAuth(res.data);
            this.toastr.success('Succesful login',null,{positionClass:'toast-bottom-right'})
          } 
        });
    }  
  }

}
