import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { FilmsPageModule } from "./_pages/films-page/films-page.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SectionsModule } from "./_features/sections/sections.module";
import { MatSidenavModule } from "@angular/material";
// import { FilmAddFormComponent } from './film-add-form/film-add-form.component';
import { StoresListComponent } from "./_features/stores/stores-list/stores-list.component";
import { SharedModule } from "./_shared/shared.module";
import { CustomerListboxModule } from "./_pages/customer-listbox/customer-listbox.module";
import { ActorsListboxComponent } from "./_pages/actors-listbox/actors-listbox.component";
import { ActorsListboxModule } from "./_pages/actors-listbox/actors-listbox.module";
import { MatButtonModule, MatDialogModule } from "@angular/material";
import { FormsModule } from "@angular/forms";

import { LoginActivateGuard } from "./guard/login-activate.guard";
import { CommonModule } from "@angular/common";
import { ToastrModule } from "ngx-toastr";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    BrowserAnimationsModule,
    SectionsModule,
    SharedModule,
    CustomerListboxModule,
    ActorsListboxModule,
    CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    MatButtonModule,
    FormsModule,
    MatDialogModule,
  ],

  providers: [LoginActivateGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
