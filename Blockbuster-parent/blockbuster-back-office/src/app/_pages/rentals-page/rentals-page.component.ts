import { Component, OnInit } from "@angular/core";
import { take } from "rxjs/operators";
import { Auth } from "src/app/_models/auth.model";
import { Filter } from "src/app/_models/helper/filter.model";
import { Store } from "src/app/_models/store/store.model";
import { CustomerService } from "src/app/_services/customer.service";
import { StoresService } from "src/app/_services/stores.service";

@Component({
  selector: "app-rentals-page",
  templateUrl: "./rentals-page.component.html",
  styleUrls: ["./rentals-page.component.css"],
})
export class RentalsPageComponent implements OnInit {
  store: Store;
  usernameFilter: Filter;
  constructor(
    private storesService: StoresService,
    private customerService: CustomerService
  ) {}

  ngOnInit() {
    const auth: Auth = JSON.parse(localStorage.AUTH);
    this.storesService
      .getStore(auth.storeId)
      .pipe(take(1))
      .subscribe((res) => {
        this.store = res.data;
      });

    this.customerService
      .getCustomersWithRequests()
      .pipe(take(1))
      .subscribe((res) => {
        this.usernameFilter = {
          values: res.data.map((customer) => customer.username),
          type: "customer",
        };
      });
  }
}
