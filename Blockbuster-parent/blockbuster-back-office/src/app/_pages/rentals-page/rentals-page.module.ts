import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RentalsPageRoutingModule } from "./rentals-page-routing.module";
import { RentalsPageComponent } from "./rentals-page.component";
import { SharedModule } from "src/app/_shared/shared.module";
import { RentalModule } from "src/app/_features/rental/rental.module";

@NgModule({
  declarations: [RentalsPageComponent],
  imports: [CommonModule, RentalsPageRoutingModule, SharedModule, RentalModule],
})
export class RentalsPageModule {}
