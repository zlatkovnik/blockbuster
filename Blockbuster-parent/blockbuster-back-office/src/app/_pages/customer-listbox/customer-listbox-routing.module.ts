import { NgModule } from '@angular/core';

import { CustomerListboxComponent } from './customer-listbox.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: "",
  component: CustomerListboxComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerListboxRoutingModule { }
