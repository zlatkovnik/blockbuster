import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerListboxComponent } from './customer-listbox.component';
import { NgMatSearchBarModule } from 'ng-mat-search-bar';

import { FormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { SharedModule } from 'src/app/_shared/shared.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule, MatIcon, MatIconModule, MatInputModule, MatSortModule, MatTableModule } from '@angular/material';
import { CustomerListboxRoutingModule } from './customer-listbox-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatListModule,
    SharedModule,
    MatGridListModule,
    MatTableModule,
    CustomerListboxRoutingModule,
    MatSortModule,
    MatButtonModule,
    NgMatSearchBarModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule

  ],
  declarations: [CustomerListboxComponent],
  exports: [CustomerListboxComponent],
})
export class CustomerListboxModule { }
