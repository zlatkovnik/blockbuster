import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { Customer } from 'src/app/_models/customer/customer';
import { CustomerService } from 'src/app/_services/customer.service';
import { PageNavigationService } from 'src/app/_services/shared/page-navigation.service';

import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PageNavigationComponent } from 'src/app/_shared/page-navigation/page-navigation.component';
import { MatDialog } from '@angular/material/dialog';
import { EditCustomerDialogComponent } from 'src/app/_shared/edit-customer-dialog/edit-customer-dialog.component';

import { debounceTime } from 'rxjs/operators';
import { PageNav } from 'src/app/_models/helper/page-nav.model';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-customer-listbox',
  templateUrl: './customer-listbox.component.html',
  styleUrls: ['./customer-listbox.component.css']
})
export class CustomerListboxComponent implements AfterViewInit, OnInit {

  customers: Customer[];
  customerLeng: Number;
  value = 'Clear me';

  searchText = new String();

  @ViewChild("customerForList", { static: false }) customerForListComponent;
  @ViewChild("pageNavigationComponent", { static: false }) pageNavigationComponent: PageNavigationComponent;


  displayedColumns: string[] = ['customerId', 'firstName', 'lastName', 'username', 'email', 'balance', 'city', 'country', 'address', 'actions'];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  constructor(private customerService: CustomerService,
    private toastr: ToastrService,
    private pageNavigation: PageNavigationService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    this.loadFirstData();
    this.pageNavigation.getObservable().subscribe((pageNav) => {
      if (!this.searchText) {
        this.loadCustomerForPage(pageNav);
      }
      else {
        this.searchCustomers();
      }

    });
  }

  loadCustomerForPage(pageNav) {
    this.customerService
      .getCustomeForPage(pageNav.page, pageNav.count)
      .subscribe(res => {
        this.customers = res.data;
        this.dataSource = new MatTableDataSource<any>(this.customers);
        this.dataSource.sort = this.sort;
      });
  }

  loadFirstData() {
    this.customerService.getTotalNumOfcustomers().subscribe(res => this.customerLeng = res);
    this.customerService.getCustomeForPage(1, 20)
      .subscribe(custome => {
        this.customers = custome.data;
        this.dataSource = new MatTableDataSource<any>(this.customers);
        this.dataSource.sort = this.sort;
      },
        (err) => {
          if (err.error.message) {
            this.dataSource = new MatTableDataSource<any>();

          } else {
            this.toastr.error("Server not responding");
          }
        }
      );

  }

  onUpdateFinished($event) {
    if ($event) {
      this.loadCustomerForPage(this.pageNavigation.pageNav);
    }
  }

  openDialog($event) {
    console.log($event.currentTarget.value);
    let idcustomer = Number($event.currentTarget.value);
    let customer: Customer;
    customer = this.customers.filter(cust => cust.customerId === idcustomer)[0];

    const dialogRef = this.dialog.open(EditCustomerDialogComponent, {
      width: '600px',
      height: '500px',
      data: {
        customerId: customer.customerId, firstName: customer.firstName, lastName: customer.lastName,
        email: customer.email, username: customer.username, balance: customer.balance
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      if (this.pageNavigation.pageNav) {
        this.loadCustomerForPage(this.pageNavigation.pageNav);
      }
      else {
        this.loadFirstData();
      }

    })
  }

  searchCustomers() {
    if (this.searchText) {
      this.customerService.
        getCustomersBySearchTextForPage(this.pageNavigation.pageNav.page, this.pageNavigation.pageNav.count, this.searchText)
        .pipe(debounceTime(1000))
        .subscribe(res => {
          if (res && res.data) {
            this.customers = res.data;
            this.dataSource = new MatTableDataSource<any>(this.customers);
            this.dataSource.sort = this.sort;
            this.customerService.getTotalNumOfCustomersBySearch(this.searchText).subscribe(res => this.customerLeng = res);
          }
        }, (err) => {
          if (err.error.message) {
            this.dataSource = new MatTableDataSource<any>();

          } else {
            this.toastr.error("Server not responding");
          }
        });
    }
  }
  filterCustomer($event) {
    this.searchText = $event.target.value;
    var firstPage = new PageNav();
    firstPage.page = 1;
    firstPage.count = this.pageNavigation.pageNav.count;
    this.pageNavigation.publishNextPage(firstPage);

    this.searchCustomers();
  }
  onSearchBarClose() {
    this.searchText = null;
    this.loadFirstData();

  }
}



