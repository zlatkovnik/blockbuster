import { RouterModule, Routes } from '@angular/router';
import { StoresPageComponent } from './stores-page.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: "",
    component: StoresPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StoresPageRoutingModule {}
