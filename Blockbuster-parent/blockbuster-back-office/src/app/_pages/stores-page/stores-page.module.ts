import { MatButtonModule } from '@angular/material';
import { StoresModule } from './../../_features/stores/stores.module';
import { StoresPageRoutingModule } from './stores-page-routing.module';
import { StoresPageComponent } from './stores-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/_shared/shared.module';

@NgModule({
  declarations: [StoresPageComponent],
  imports: [
    CommonModule,
    StoresPageRoutingModule,
    StoresModule,
    SharedModule,
    MatButtonModule
  ],
  exports: [StoresPageComponent]
})
export class StoresPageModule { }
