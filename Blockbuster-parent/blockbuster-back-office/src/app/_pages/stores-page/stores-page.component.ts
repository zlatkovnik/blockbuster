import { StoresAddDialogComponent } from './../../_features/stores/stores-add-dialog/stores-add-dialog.component';
import { MatDialog } from '@angular/material';
import { take } from "rxjs/operators";
import { AuthService } from "./../../_services/auth.service";
import { Auth } from "./../../_models/auth.model";
import { StoresService } from "./../../_services/stores.service";
import { Component, OnInit } from "@angular/core";
import { Filter } from "src/app/_models/helper/filter.model";
import { FilterService } from "src/app/_services/shared/filter.service";
import { Subscription } from "rxjs";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-stores-page",
  templateUrl: "./stores-page.component.html",
  styleUrls: ["./stores-page.component.css"],
})
export class StoresPageComponent implements OnInit {
  stores: any[];
  cities: string[];
  auth: Auth;
  staffStoreId: number = 0;
  staffId: number = 0;

  cityFilter: Filter = { type: "City", values: ["All"] };

  currentFilter = this.cityFilter.values[0];

  private subscriptions: Subscription[] = [];

  constructor(
    private storesService: StoresService,
    private authService: AuthService,
    private filterService: FilterService,
    private toastr: ToastrService,
    public dialog: MatDialog
  ) {}

  openAddDialog() {
    const dialogRef = this.dialog.open(StoresAddDialogComponent);

    dialogRef.afterClosed().subscribe((result) =>
    { this.loadStores();
      this.storesService
      .getCities()
      .pipe(take(1))
      .subscribe(
        (res) => {
          this.cityFilter.values = [
            'All',
            ...res.map((c) => c),
          ];
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
    });
  }

  ngOnInit() {
    const auth = localStorage.AUTH;

    if (auth) {
      this.auth = JSON.parse(auth);
      this.staffStoreId = this.auth.storeId;
      this.staffId = this.auth.staffId;
    }

    this.authService.getAuthObservable().subscribe((auth) => {
      this.auth = auth;
      if (auth) {
        this.staffStoreId = auth.storeId;
        this.staffId = auth.staffId;
        this.loadStores();
      }
    });

    this.storesService
      .getStores()
      .pipe(take(1))
      .subscribe(
        (res) => {
          this.stores = res;
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );

    this.storesService
      .getCities()
      .pipe(take(1))
      .subscribe(
        (res) => {
          this.cityFilter.values = [
            ...this.cityFilter.values,
            ...res.map((c) => c),
          ];
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );

    this.subscriptions.push(
      this.filterService.getObservable().subscribe((filter) => {
        if (filter.type === "City") {
          this.currentFilter = filter.filterValue;
          this.loadStores();
        }
      })
    );
  }

  private loadStores() {
    if (this.currentFilter == "All") {
      this.storesService.getStores().subscribe(
        (res) => {
          this.stores = res;
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
    } else {
      this.storesService.getStoresByCity(this.currentFilter).subscribe(
        (res) => {
          this.stores = res;
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
    }
  }
}
