import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { StaffPageRoutingModule } from "./staff-page-routing.module";
import { StaffPageComponent } from "./staff-page.component";
import { StaffModule } from "src/app/_features/staff/staff.module";

@NgModule({
  declarations: [StaffPageComponent],
  imports: [CommonModule, StaffPageRoutingModule, StaffModule],
  exports: [StaffPageComponent],
})
export class StaffPageModule {}
