import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { StaffPageComponent } from "./staff-page.component";

const routes: Routes = [
  {
    path: "",
    component: StaffPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StaffPageRoutingModule {}
