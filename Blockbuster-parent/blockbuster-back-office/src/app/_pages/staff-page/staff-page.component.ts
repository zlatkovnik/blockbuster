import { Staff } from './../../_models/staff/staff-for-list';
import { StaffService } from './../../_services/staff.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-staff-page',
  templateUrl: './staff-page.component.html',
  styleUrls: ['./staff-page.component.css']
})
export class StaffPageComponent implements OnInit {

  staff: Staff[];

  constructor(
    private staffService: StaffService,
  ) { }

  ngOnInit() {
    this.staffService
      .getStaff()
      .subscribe((res) => (this.staff = res.data));
  }

}
