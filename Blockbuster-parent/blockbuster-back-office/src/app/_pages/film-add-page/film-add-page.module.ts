import { FilmModule } from 'src/app/_features/film/film.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilmAddPageRoutingModule } from './film-add-page-routing.module';
import { FilmAddPageComponent } from './film-add-page.component';


@NgModule({
  declarations: [FilmAddPageComponent],
  imports: [
    CommonModule,
    FilmAddPageRoutingModule,
    FilmModule
  ]
})
export class FilmAddPageModule { }
