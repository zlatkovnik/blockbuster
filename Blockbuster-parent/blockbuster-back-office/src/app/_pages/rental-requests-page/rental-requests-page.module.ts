import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RentalRequestsPageRoutingModule } from "./rental-requests-page-routing.module";
import { RentalRequestsPageComponent } from "./rental-requests-page.component";
import { RentalModule } from "src/app/_features/rental/rental.module";
import { SharedModule } from "src/app/_shared/shared.module";

@NgModule({
  declarations: [RentalRequestsPageComponent],
  imports: [
    CommonModule,
    RentalRequestsPageRoutingModule,
    RentalModule,
    SharedModule,
  ],
})
export class RentalRequestsPageModule {}
