import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { take } from "rxjs/operators";
import { Auth } from "src/app/_models/auth.model";
import { RentalRequest } from "src/app/_models/film/rental-request.model";
import { Filter } from "src/app/_models/helper/filter.model";
import { Store } from "src/app/_models/store/store.model";
import { CustomerService } from "src/app/_services/customer.service";
import { RentalService } from "src/app/_services/film/rental.service";
import { PageNavigationService } from "src/app/_services/shared/page-navigation.service";
import { StoresService } from "src/app/_services/stores.service";

@Component({
  selector: "app-rental-requests-page",
  templateUrl: "./rental-requests-page.component.html",
  styleUrls: ["./rental-requests-page.component.css"],
})
export class RentalRequestsPageComponent implements OnInit {
  auth: Auth = JSON.parse(localStorage.AUTH);
  myStore: Store;
  rentalRequests: RentalRequest[] = [];
  customerFilter: {
    type: string;
    values: string[];
  };
  statusFilter = {
    type: "status",
    values: ["all", "pending", "accepted", "rejected", "canceled"],
  };

  constructor(
    private customerService: CustomerService,
    private storeService: StoresService
  ) {}

  ngOnInit() {
    this.customerService
      .getCustomersWithRequests()
      .pipe(take(1))
      .subscribe((res) => {
        this.customerFilter = {
          type: "customer",
          values: res.data.map((c) => c.username),
        };
      });

    this.storeService
      .getStore(this.auth.storeId)
      .pipe(take(1))
      .subscribe((res) => {
        this.myStore = res.data;
      });
  }
}
