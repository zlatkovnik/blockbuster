import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RentalRequestsPageComponent } from "./rental-requests-page.component";

const routes: Routes = [
  {
    path: "",
    component: RentalRequestsPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RentalRequestsPageRoutingModule {}
