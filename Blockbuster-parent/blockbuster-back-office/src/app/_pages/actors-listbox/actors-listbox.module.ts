import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActorsListboxComponent } from './actors-listbox.component';
import { SharedModule } from 'src/app/_shared/shared.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule } from '@angular/material';
import { ActorListboxRoutingModule } from './actor-listbox-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatGridListModule,
    MatListModule,
    ActorListboxRoutingModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,

  ],
  declarations: [ActorsListboxComponent]
})
export class ActorsListboxModule { }
