import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { debounceTime } from 'rxjs/operators';
import { Actor } from 'src/app/_models/actor/actor';
import { PageNav } from 'src/app/_models/helper/page-nav.model';
import { ActorService } from 'src/app/_services/actor.service';
import { PageNavigationService } from 'src/app/_services/shared/page-navigation.service';



@Component({
  selector: 'app-actors-listbox',
  templateUrl: './actors-listbox.component.html',
  styleUrls: ['./actors-listbox.component.css']
})
export class ActorsListboxComponent implements OnInit {

  actors: Actor[];
  actorNum: Number;

  value = 'Clear me';
  searchText = new String();

  constructor(public actorService: ActorService,
    private pageNavigation: PageNavigationService,
    private toastr: ToastrService,

  ) {
  }
  ngOnInit() {
    this.loadFirstData();

    this.pageNavigation.getObservable().subscribe((pageNav) => {
      if (!this.searchText) {
        this.loadDataForPage(pageNav);
      }
      else {
        this.searchActors();
      }
    });
  }

  loadDataForPage(pageNav) {
    this.actorService
      .getActorsForPage(pageNav.page, pageNav.count)
      .subscribe((res) => (this.actors = res.data));

  }

  loadFirstData() {
    this.actorService.getTotalNumOfActors().subscribe(res =>
      this.actorNum = res);
    this.actorService.getActorsForPage(1, 20)
      .subscribe(act => this.actors = act.data);
  }

  filterActors($event) {
    this.searchText = $event.target.value;
    var firstPage = new PageNav();
    firstPage.page = 1;
    firstPage.count = this.pageNavigation.pageNav.count;
    this.pageNavigation.publishNextPage(firstPage);
    this.searchActors();
  }

  searchActors() {
    if (this.searchText) {
      this.actorService.
        getActorsBySearchForPage(this.pageNavigation.pageNav.page, this.pageNavigation.pageNav.count, this.searchText)
        .pipe(debounceTime(1000))
        .subscribe(res => {
          if (res && res.data) {
            this.actors = res.data;
            this.actorService.getTotalNumOfActorBySearchText(this.searchText)
              .subscribe(res => {
                this.actorNum = res;
              }, (err) => {
                if (err.error.message) {
                  this.actors = null;

                } else {
                  this.toastr.error("Server not responding");
                }
              });
          }
        });
    }
  }

  onSearchBarClose() {
    this.searchText = null;
    this.loadFirstData();

  }


}
