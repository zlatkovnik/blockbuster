import { Auth } from "./../../_models/auth.model";
import { AuthService } from "./../../_services/auth.service";
import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.css"],
})
export class HomePageComponent implements OnInit {
  auth: Auth = undefined;

  cards = [
    { title: "stores", route: "/stores", iconName: "store" },
    { title: "actors", route: "/actors", iconName: "person" },
    { title: "films", route: "/films", iconName: "movie" },
    { title: "staff", route: "/staff", iconName: "engineering" },
    { title: "customers", route: "/customers", iconName: "hail" },
    { title: "Add Film", route: "/addFilm", iconName: "movie_filter" },
    { title: "Rentals", route: "/rentals", iconName: "album" },
    {
      title: "Rental Requests",
      route: "/rentals/requests",
      iconName: "pending_actions",
    },
    {
      title: "hide",
      route: "",
      iconName: "",
    },
  ];

  constructor(
    private authService: AuthService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    const auth = localStorage.AUTH;

    if (auth) {
      this.auth = JSON.parse(auth);
    }

    this.authService
      .getAuthObservable()
      .subscribe((auth) => (this.auth = auth));
  }
}
