import { SharedModule } from "src/app/_shared/shared.module";
import { LoginModule } from "./../../_features/login/login.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomePageRoutingModule } from "./home-page-routing.module";
import { HomePageComponent } from "./home-page.component";

@NgModule({
  declarations: [HomePageComponent],
  imports: [CommonModule, HomePageRoutingModule, LoginModule, SharedModule],
  exports: [HomePageComponent],
})
export class HomePageModule {}
