import { Component, OnDestroy, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { first, take, tap } from "rxjs/operators";
import { FilmsForList } from "src/app/_models/film/films-for-list.model";
import { Filter } from "src/app/_models/helper/filter.model";
import { PageNav } from "src/app/_models/helper/page-nav.model";
import { Sort } from "src/app/_models/helper/sort.model";
import { CategoryService } from "src/app/_services/film/category.service";
import { FilmService } from "src/app/_services/film/film.service";
import { FilterService } from "src/app/_services/shared/filter.service";
import { PageNavigationService } from "src/app/_services/shared/page-navigation.service";
import { SortService } from "src/app/_services/shared/sort.service";

@Component({
  selector: "app-films-page",
  templateUrl: "./films-page.component.html",
  styleUrls: ["./films-page.component.css"],
})
export class FilmsPageComponent implements OnInit, OnDestroy {
  sorts: Sort[] = [
    {
      text: "Best",
      value: "best",
    },
    {
      text: "Worst",
      value: "worst",
    },
    {
      text: "A-Z",
      value: "a-z",
    },
    {
      text: "Z-A",
      value: "z-a",
    },
    {
      text: "Newest",
      value: "newest",
    },
    {
      text: "Oldest",
      value: "oldest",
    },
  ];
  categoryFilter: Filter = { type: "Category", values: ["All"] };

  films: FilmsForList;
  currentSort = this.sorts[0].value;
  currentFilter = this.categoryFilter.values[0];
  currentPage: PageNav = {
    count: 20,
    page: 1,
  };
  private subscriptions: Subscription[] = [];

  constructor(
    private filmService: FilmService,
    private pageNavigation: PageNavigationService,
    private sortService: SortService,
    private filterService: FilterService,
    private categoryService: CategoryService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadSortedFilteredFilms();

    this.subscriptions.push(
      this.pageNavigation.getObservable().subscribe((pageNav) => {
        this.currentPage = pageNav;
        this.loadSortedFilteredFilms();
      })
    );

    this.subscriptions.push(
      this.sortService.getObservable().subscribe((sort) => {
        this.currentSort = sort;
        this.loadSortedFilteredFilms();
      })
    );

    this.subscriptions.push(
      this.filterService.getObservable().subscribe((filter) => {
        if (filter.type === "Category") {
          this.currentFilter = filter.filterValue;
          this.loadSortedFilteredFilms();
        }
      })
    );

    this.categoryService
      .getAllCategories()
      .pipe(take(1))
      .subscribe(
        (res) => {
          this.categoryFilter.values = [
            ...this.categoryFilter.values,
            ...res.data.map((c) => c.name),
          ];
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  private loadSortedFilteredFilms() {
    this.filmService
      .getFilmsForListPage(
        this.currentPage,
        this.currentSort,
        this.currentFilter
      )
      .pipe(take(1))
      .subscribe(
        (res) => (this.films = res.data),
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
