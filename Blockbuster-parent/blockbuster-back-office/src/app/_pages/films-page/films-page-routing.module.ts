import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FilmsPageComponent } from "./films-page.component";

const routes: Routes = [
  {
    path: "",
    component: FilmsPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FilmsPageRoutingModule {}
