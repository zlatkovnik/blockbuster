import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FilmsPageRoutingModule } from "./films-page-routing.module";
import { SharedModule } from "src/app/_shared/shared.module";
import { MatButtonModule } from "@angular/material";
import { FilmsPageComponent } from "./films-page.component";
import { FilmModule } from "src/app/_features/film/film.module";

@NgModule({
  declarations: [FilmsPageComponent],
  imports: [
    CommonModule,
    FilmsPageRoutingModule,
    SharedModule,
    MatButtonModule,
    FilmModule,
  ],
  exports: [FilmsPageComponent],
})
export class FilmsPageModule { }
