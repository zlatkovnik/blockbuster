import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { InventoryPageRoutingModule } from "./inventory-page-routing.module";
import { InventoryPageComponent } from "./inventory-page.component";
import { SharedModule } from "src/app/_shared/shared.module";

@NgModule({
  declarations: [InventoryPageComponent],
  imports: [CommonModule, InventoryPageRoutingModule, SharedModule],
  exports: [InventoryPageComponent],
})
export class InventoryPageModule {}
