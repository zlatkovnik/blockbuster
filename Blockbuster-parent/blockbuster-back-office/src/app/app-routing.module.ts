import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginActivateGuard } from "./guard/login-activate.guard";

const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  {
    path: "home",
    loadChildren: () =>
      import("./_pages/home-page/home-page.module").then(
        (m) => m.HomePageModule
      ),
  },
  {
    path: "films",
    loadChildren: () =>
      import("./_pages/films-page/films-page.module").then(
        (m) => m.FilmsPageModule
      ),
    canActivate: [LoginActivateGuard],
  },
  {
    path: "staff",
    loadChildren: () =>
      import("./_pages/staff-page/staff-page.module").then(
        (m) => m.StaffPageModule
      ),
    canActivate: [LoginActivateGuard],
  },
  {
    path: "addFilm",
    loadChildren: () =>
      import("./_pages/film-add-page/film-add-page.module").then(
        (m) => m.FilmAddPageModule
      ),
    canActivate: [LoginActivateGuard],
  },
  {
    path: "stores",
    loadChildren: () =>
      import("./_pages/stores-page/stores-page.module").then(
        (m) => m.StoresPageModule
      ),
    canActivate: [LoginActivateGuard],
  },
  {
    path: "inventory",
    loadChildren: () =>
      import("./_pages/inventory-page/inventory-page.module").then(
        (m) => m.InventoryPageModule
      ),
    canActivate: [LoginActivateGuard],
  },
  {
    path: "customers",
    loadChildren: () =>
      import("./_pages/customer-listbox/customer-listbox.module").then(
        (m) => m.CustomerListboxModule
      ),
    canActivate: [LoginActivateGuard],
  },
  {
    path: "actors",
    loadChildren: () =>
      import("./_pages/actors-listbox/actors-listbox.module").then(
        (m) => m.ActorsListboxModule
      ),
    canActivate: [LoginActivateGuard],
  },
  {
    path: "rentals",
    loadChildren: () =>
      import("./_pages/rentals-page/rentals-page.module").then(
        (m) => m.RentalsPageModule
      ),
    canActivate: [LoginActivateGuard],
  },
  {
    path: "rentals/requests",
    loadChildren: () =>
      import("./_pages/rental-requests-page/rental-requests-page.module").then(
        (m) => m.RentalRequestsPageModule
      ),
    canActivate: [LoginActivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
