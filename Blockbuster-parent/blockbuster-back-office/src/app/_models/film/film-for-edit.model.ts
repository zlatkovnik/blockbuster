export interface FilmForEdit {
  filmId: number;
  title: string;
  description: string;
  releaseYear: number;
  languageId: number;
  categoryId: number;
  rentalDuration: number;
  rentalRate: number;
  length: number;
  replacementCost: number;
  rating: string;
  starRating: number;
  posterUrl: string;
  availableInventory: number;
  totalInventory: number;
}
