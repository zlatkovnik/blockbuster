export class FilmForAdd {
  title: string;
  description: string;
  releaseYear: number;
  rentalDuration: string;
  rentalRate: string;
  length: number;
  replacementCost: number;
  posterUrl: string;
  starRating: string;
  languageId: number;
  categoryId: number;
  moviedbId: number;
}
