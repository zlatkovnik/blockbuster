import { FilmForList } from "./film-for-list.model";

export interface FilmsForList {
  films: FilmForList[];
  numberOfRecords: number;
}
