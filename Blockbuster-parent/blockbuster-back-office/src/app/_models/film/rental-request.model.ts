export interface RentalRequest {
  rentalRequestId: number;
  filmId: number;
  title: string;
  customerId: number;
  username: string;
  inventoryCount: number;
  status: "pending" | "rejected" | "accepted" | "canceled" | "expired";
  createDate: Date;
}
