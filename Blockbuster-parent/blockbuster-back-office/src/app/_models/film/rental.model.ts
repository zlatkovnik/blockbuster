export interface Rental {
  rentalId: number;
  rentalDate: Date;
  customerId: number;
  filmId: number;
  title: string;
  posterUrl: string;
  returnDate: Date;
}
