export interface Inventory {
  inventoryId: number;
  filmId: number;
  storeId: number;
}
