import { FilmForList } from "../film/film-for-list.model";

export class FilmsForActor {
  [x: string]: any;
  filmId: number;
  actorId: number;
  name: string;
  filmsForList: FilmForList[];
}
