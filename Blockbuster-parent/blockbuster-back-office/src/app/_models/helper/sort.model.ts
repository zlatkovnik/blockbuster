export interface Sort {
  text: string;
  value: string;
}
