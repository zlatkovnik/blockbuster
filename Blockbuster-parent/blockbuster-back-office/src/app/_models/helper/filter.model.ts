export interface Filter {
  type: string;
  values: string[];
}
