export interface Staff {
    staffId: number;
    firstName: string;
    lastName: string;
    email: string;
    username: string;
    pictureUrl: string;
  }
  