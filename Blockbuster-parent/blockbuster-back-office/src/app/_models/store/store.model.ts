export interface Store {
  storeId: number;
  address: string;
  city: string;
  country: string;
  longitude: number;
  latitude: number;
  postalCode: string;
}
