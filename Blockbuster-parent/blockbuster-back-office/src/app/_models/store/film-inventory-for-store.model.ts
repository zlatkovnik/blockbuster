export interface FilmInventoryForStore {
  storeId: number;
  address: string;
  inventoryCount: number;
}
