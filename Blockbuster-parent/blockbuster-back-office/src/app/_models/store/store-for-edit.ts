import { Country } from './../address/country';
export interface StoreForEdit {
    storeId: number;
    addressId: number;
    address: string;
    district: string;
    city: string;
    country: string;
    cityId: number;
    countryId: number;
    longitude: number;
    latitude: number;
    postalCode: string;
    managerStaffId: number;
  }
  