export interface ActorMoviedb {
  id: number;
  name: string;
  profile_path: string;
  character: string;
}
