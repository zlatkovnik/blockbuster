export interface Actor {
  actorId: string;
  name: string;
}
