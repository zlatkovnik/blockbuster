export class Customer {
  customerId: number;
  avatarUrl: string;
  username: string;
  balance: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  address: string;
  city: string;
  country: string;
  district: string;
  postalCode: string;

}
