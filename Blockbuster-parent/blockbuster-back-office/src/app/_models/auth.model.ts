export interface Auth {
    staffId: number;
    email: string;
    username: string;
    picture: string;
    active: boolean;
    firstName: string;
    lastName: string;
    storeId: number;
    addressId: number;
  }