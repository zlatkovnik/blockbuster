import { Component, Input, OnInit } from '@angular/core';
import { FilmForList } from 'src/app/_models/film/film-for-list.model';

@Component({
  selector: 'app-films-for-add-to-actor-list',
  templateUrl: './films-for-add-to-actor-list.component.html',
  styleUrls: ['./films-for-add-to-actor-list.component.css']
})
export class FilmsForAddToActorListComponent implements OnInit {

  @Input("films")
  films: FilmForList[];


  constructor() { }

  ngOnInit() {
  }


}
