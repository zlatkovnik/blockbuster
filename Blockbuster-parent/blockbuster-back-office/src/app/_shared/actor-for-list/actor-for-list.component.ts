import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Actor } from "src/app/_models/actor/actor";
import { FilmForList } from "src/app/_models/film/film-for-list.model";
import { ActorService } from "src/app/_services/actor.service";
import { FilmService } from "src/app/_services/film.service";
import { EditActorComponent } from "../edit-actor/edit-actor.component";

@Component({
  selector: "app-actor-for-list",
  templateUrl: "./actor-for-list.component.html",
  styleUrls: ["./actor-for-list.component.css"],
})
export class ActorForListComponent implements OnInit {
  panelOpenState = false;

  @Input("actor")
  actor: Actor;

  @ViewChild("filmsForActor", { static: false }) filmsForActorComponent;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openDialog(): void {
    const dialogRef = this.dialog.open(EditActorComponent, {
      width: "800px",
      height: "600px",

      data: {
        actorId: this.actor.actorId,
        name: this.actor.name,
      },
    });

    dialogRef.afterClosed().subscribe((res) => {
      if (res === true) {
        this.filmsForActorComponent.showFilmsForActor();
      }
    });
  }
}
