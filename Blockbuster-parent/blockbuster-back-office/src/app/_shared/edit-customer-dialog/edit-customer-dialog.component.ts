import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { take } from "rxjs/operators";
import { Customer } from 'src/app/_models/customer/customer';
import { CustomerService } from 'src/app/_services/customer.service';

@Component({
  selector: 'app-edit-customer-dialog',
  templateUrl: './edit-customer-dialog.component.html',
  styleUrls: ['./edit-customer-dialog.component.css']
})
export class EditCustomerDialogComponent implements OnInit {


  constructor(private toastr: ToastrService,
    private customerService: CustomerService,
    public dialogRef: MatDialogRef<EditCustomerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
  }

  ngOnInit() {

  }

  onSubmitClick() {
    let customerForEdit = new Customer();
    customerForEdit.customerId = this.data.customerId;
    customerForEdit.firstName = this.data.firstName;
    customerForEdit.lastName = this.data.lastName;
    customerForEdit.email = this.data.email;
    customerForEdit.username = this.data.username;
    customerForEdit.balance = this.data.balance;
    this.customerService.updateCustomer(customerForEdit)
      .pipe(take(1))
      .subscribe(response => {
        this.toastr.success(`Successfully edited '${customerForEdit.firstName}'`);
      },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        });
    this.dialogRef.close();
  }

  onCloseClick() {
    this.dialogRef.close();
  }
}
