import { Component, Input, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { Filter } from "src/app/_models/helper/filter.model";
import { FilterService } from "src/app/_services/shared/filter.service";

@Component({
  selector: "app-search-select-filter",
  templateUrl: "./search-select-filter.component.html",
  styleUrls: ["./search-select-filter.component.css"],
})
export class SearchSelectFilterComponent implements OnInit {
  @Input() filter: Filter;
  search = new FormControl();
  filteredOptions: Observable<string[]>;

  constructor(private filterService: FilterService) {}

  ngOnInit() {
    this.filteredOptions = this.search.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.filter.values.filter(
      (option) => option.toLowerCase().indexOf(filterValue) === 0
    );
  }

  onClick(filterValue: string) {
    this.filterService.publishFilter(this.filter.type, filterValue);
  }
}
