import { Component, Input, OnInit } from "@angular/core";
import { Filter } from "src/app/_models/helper/filter.model";
import { FilterService } from "src/app/_services/shared/filter.service";

@Component({
  selector: "app-select-filters",
  templateUrl: "./select-filters.component.html",
  styleUrls: ["./select-filters.component.css"],
})
export class SelectFiltersComponent implements OnInit {
  @Input() filter: Filter;

  constructor(private filterService: FilterService) { }

  ngOnInit() { }

  onClick(filterValue: string) {
    this.filterService.publishFilter(this.filter.type, filterValue);
  }
}
