import { NavCardComponent } from "./nav-card/nav-card.component";
import { MatButtonModule } from "@angular/material/button";
import { RouterModule } from "@angular/router";
import { MatIconModule } from "@angular/material/icon";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PageNavigationComponent } from "./page-navigation/page-navigation.component";
import {
  MatAutocompleteModule,
  MatCheckbox,
  MatCheckboxModule,
  MatIcon,
  MatPaginatorModule,
  MatTableModule,
} from "@angular/material";
import { MatListModule } from "@angular/material/list";
import { MatCardModule } from "@angular/material/card";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatButtonToggleModule } from "@angular/material";

import { ActorForListComponent } from "./actor-for-list/actor-for-list.component";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { FilmsForActorComponent } from "./films-for-actor/films-for-actor.component";
import { ToggleSortsComponent } from "./toggle-sorts/toggle-sorts.component";
import { SelectFiltersComponent } from "./select-filters/select-filters.component";
import { ShowFilmsSuggestionComponent } from "./show-films-suggestion/show-films-suggestion.component";

import { HumanizeTimePipe } from "./pipes/humanize-time.pipe";

import { EditActorComponent } from "./edit-actor/edit-actor.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatSidenavModule } from "@angular/material/sidenav";

import { MatSortModule } from "@angular/material/sort";

import {
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
} from "@angular/material";
import { EditCustomerDialogComponent } from "./edit-customer-dialog/edit-customer-dialog.component";
import { HumanizeDatePipe } from "./pipes/humanize-date.pipe";
import { SearchSelectFilterComponent } from "./search-select-filter/search-select-filter.component";

@NgModule({
  declarations: [
    PageNavigationComponent,
    ActorForListComponent,
    FilmsForActorComponent,
    ToggleSortsComponent,
    SelectFiltersComponent,
    EditActorComponent,
    ShowFilmsSuggestionComponent,
    NavCardComponent,
    HumanizeTimePipe,
    EditCustomerDialogComponent,
    HumanizeDatePipe,
    SearchSelectFilterComponent,
  ],
  imports: [
    CommonModule,
    MatPaginatorModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatGridListModule,
    MatButtonToggleModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatInputModule,
    FormsModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    RouterModule,
    MatButtonModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
  ],
  exports: [
    PageNavigationComponent,
    ToggleSortsComponent,
    ActorForListComponent,
    FilmsForActorComponent,
    PageNavigationComponent,
    ToggleSortsComponent,
    SelectFiltersComponent,
    EditActorComponent,
    ShowFilmsSuggestionComponent,
    HumanizeTimePipe,
    PageNavigationComponent,
    ToggleSortsComponent,
    SelectFiltersComponent,
    NavCardComponent,
    EditCustomerDialogComponent,
    HumanizeDatePipe,
    SearchSelectFilterComponent,
  ],
  entryComponents: [EditActorComponent, EditCustomerDialogComponent],
})
export class SharedModule { }
