import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Input, OnInit, Output } from '@angular/core';
import { cpuUsage } from 'process';
import { Subject } from 'rxjs';
import { subscribeOn } from 'rxjs/operators';
import { Actor } from 'src/app/_models/actor/actor';
import { FilmForAdd } from 'src/app/_models/film/film-for-add.model';
import { FilmForList } from 'src/app/_models/film/film-for-list.model';
import { FilmsForList } from 'src/app/_models/film/films-for-list.model';
import { FilmService } from 'src/app/_services/film.service';

@Component({
  selector: 'app-show-films-suggestion',
  templateUrl: './show-films-suggestion.component.html',
  styleUrls: ['./show-films-suggestion.component.css']
})
export class ShowFilmsSuggestionComponent implements OnInit {

  showSpiner = false;

  @Input("actor")
  actor: Actor;

  @Input('mode')
  mode: string; //adding or preview

  filmsForAdd: FilmForList[];


  @Input("films")
  films: FilmForList[];

  @Output()//ime kategorije za slider
  filmChecked = new Subject();

  @Input("actorId")
  actorId;

  constructor(private filmService: FilmService) {

    this.films = new Array<FilmForList>();
    this.filmsForAdd = new Array<FilmForList>();

  }
  ngOnInit() {
    if (this.actorId) {
      this.showSpiner = true;

      this.filmService.getFilmsSuggestionForActor(this.actorId)
        .subscribe(res => {
          this.films = res;

        });
    }
  }

  onFilmClick() {
  }

  onClickedCheckbox($event) {
    let filmID = Number($event.target.id);

    let filmForAdding = this.films.filter(film => film.filmId === filmID)[0] as FilmForList;


    if (this.filmsForAdd.includes(filmForAdding)) {

      this.filmsForAdd.forEach((film, index) => {
        if (film === filmForAdding) this.filmsForAdd.splice(index, 1);
      });



    }
    else {
      this.filmsForAdd.push(filmForAdding);

    }

  }

  onClickAddFilmToList() {
    this.filmChecked.next(this.filmsForAdd);
  }

  isFilmForAdding(film: FilmForList) {
    return this.filmsForAdd.includes(film);

  }
}
