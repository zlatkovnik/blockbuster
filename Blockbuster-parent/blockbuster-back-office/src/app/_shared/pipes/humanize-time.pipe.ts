import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "humanizeTime",
})
export class HumanizeTimePipe implements PipeTransform {
  transform(value: number): string {
    const hours = value / 60;
    const rHours = Math.floor(hours);
    const minutes = (hours - rHours) * 60;
    var rMinutes = Math.round(minutes);
    if (rMinutes === 0) return `${rHours}h`;
    return rHours > 0 ? `${rHours}h ${rMinutes}min` : `${rMinutes}min`;
  }
}
