import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, Input, OnInit } from "@angular/core";
import { fakeAsync } from "@angular/core/testing";
import { Sort } from "@angular/material/sort";
import { Actor } from "src/app/_models/actor/actor";
import { FilmForList } from "src/app/_models/film/film-for-list.model";
import { ActorService } from "src/app/_services/actor.service";
import { FilmService } from "src/app/_services/film/film.service";
import { take } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-films-for-actor",
  templateUrl: "./films-for-actor.component.html",
  styleUrls: ["./films-for-actor.component.css"],
})
export class FilmsForActorComponent implements OnInit {
  @Input("actor")
  actor: Actor;

  private _panelOpenState: boolean;
  @Input("panelOpenState")
  set panelOpenState(value: boolean) {
    this._panelOpenState = value;
    if (value) {
      this.showFilmsForActor();
    }
  }
  get panelOpenState(): boolean {
    return this._panelOpenState;
  }

  sortedData: FilmForList[];

  films: FilmForList[];

  constructor(
    private toastr: ToastrService,
    private filmService: FilmService,
    private actorService: ActorService) {

  }

  ngOnInit() {
  }

  showFilmsForActor() {
    if (this.actor) {
      this.filmService
        .getFilmsForActor(this.actor.actorId)
        .subscribe(res => {
          this.films = res;
          this.sortedData = this.films;
        });
    }

  }

  sortData(sort: Sort) {

    const data = this.films.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'customerId': return compare(a.filmId, b.filmId, isAsc);
        case 'title': return compare(a.title, b.title, isAsc);
        case 'category': return compare(a.categoryName, b.categoryName, isAsc);
        case 'rating': return compare(a.starRating, b.starRating, isAsc);
        case 'delete': return 0;
        default: return compare(a.filmId, b.filmId, isAsc);
      }
    });
  }

  onDeleteClick($event) {
    let filmID = Number($event.currentTarget.value);
    this.actorService.deleteFilmForActor(filmID, this.actor.actorId)
      .pipe(take(1))
      .subscribe(response => {
        this.toastr.success(`Successfully deleted film '${filmID}'`);
      },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        });;
    this.panelOpenState = true;
    this.showFilmsForActor();
  }

}


function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
