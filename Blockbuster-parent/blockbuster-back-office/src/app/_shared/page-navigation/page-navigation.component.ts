import { Component, Input, OnInit } from "@angular/core";
import { PageEvent } from "@angular/material";
import { PageNav } from "src/app/_models/helper/page-nav.model";
import { PageNavigationService } from "src/app/_services/shared/page-navigation.service";

@Component({
  selector: "app-page-navigation",
  templateUrl: "./page-navigation.component.html",
  styleUrls: ["./page-navigation.component.css"],
})
export class PageNavigationComponent implements OnInit {
  @Input() length: number;

  constructor(private pageNavigation: PageNavigationService) { }

  ngOnInit() { }

  handlePageEvent(event: PageEvent) {
    const pageNav: PageNav = {
      page: event.pageIndex + 1,
      count: event.pageSize,
    };
    this.pageNavigation.publishNextPage(pageNav);
  }
}
