import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-card',
  templateUrl: './nav-card.component.html',
  styleUrls: ['./nav-card.component.css']
})
export class NavCardComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  iconName: string;

  @Input()
  route: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
