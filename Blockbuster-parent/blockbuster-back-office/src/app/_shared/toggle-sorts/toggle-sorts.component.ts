import { Component, Input, OnInit } from "@angular/core";
import { Sort } from "src/app/_models/helper/sort.model";
import { SortService } from "src/app/_services/shared/sort.service";

@Component({
  selector: "app-toggle-sorts",
  templateUrl: "./toggle-sorts.component.html",
  styleUrls: ["./toggle-sorts.component.css"],
})
export class ToggleSortsComponent implements OnInit {
  @Input() sorts: Sort[];

  constructor(private sortService: SortService) {}

  ngOnInit() {}

  onClick(sort: string) {
    this.sortService.publishSort(sort);
  }
}
