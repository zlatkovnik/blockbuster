import { Component, Input, OnInit, Output, ViewChild } from "@angular/core";
import { Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { Actor } from "src/app/_models/actor/actor";
import { FilmForList } from "src/app/_models/film/film-for-list.model";
import { FilmsForActor } from "src/app/_models/filmsForActors/filmsForActors";
import { take } from "rxjs/operators";

import { ActorService } from "src/app/_services/actor.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-edit-actor",
  templateUrl: "./edit-actor.component.html",
  styleUrls: ["./edit-actor.component.css"],
})
export class EditActorComponent implements OnInit {
  @Input("actor")
  actor: Actor;
  mode: string; //adding or preview
  actorId: Number;
  filmsForAdd;
  filmsDorDisplay: FilmForList[];
  @ViewChild("drawer", { static: true }) drawer;
  showFiller = false;
  constructor(
    private toastr: ToastrService,
    private actorService: ActorService,
    public dialogRef: MatDialogRef<EditActorComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    this.actorId = this.data.actorId;
  }

  onClickNoOk(): void {
    this.dialogRef.close();
  }
  onSubmitClick() {
    let actorAndFilms = new FilmsForActor();
    actorAndFilms.name = this.data.name;
    actorAndFilms.actorId = this.data.actorId;
    actorAndFilms.filmsForList = this.filmsForAdd;
    this.actorService
      .updateActor(actorAndFilms)
      .pipe(take(1))
      .subscribe(
        (response) => {
          this.toastr.success(`Successfully edited '${actorAndFilms.name}'`);
        },
        (err) => {
          if (err.error.message) {
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error("Server not responding");
          }
        }
      );

    this.dialogRef.close(true);
  }

  onClickAddFilmToList() {
    !this.drawer.toggle();
  }

  openFilmsForAdd() {
    this.mode = "adding";
    this.filmsForAdd = null;
    this.drawer.toggle();
  }

  getFilmChacked($event) {
    !this.drawer.toggle();

    if ($event) {
      this.filmsForAdd = $event;
      this.mode = "preview";
    } else {
      this.filmsForAdd = null;
    }
  }

  onCloseCLick() {
    this.dialogRef.close();
  }
}
