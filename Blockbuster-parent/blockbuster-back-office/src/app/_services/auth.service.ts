import { Subject } from "rxjs";
import { Auth } from "./../_models/auth.model";
import { Response } from "./../_models/template/response.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { DomainService } from "./shared/domain.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  url = this.domain.getAddress() + "/auth";
  private authSubject: Subject<Auth> = new Subject<Auth>();

  constructor(private http: HttpClient, private domain: DomainService) { }

  login(email: string, password: string): Observable<Response<Auth>> {
    return this.http.post<Response<Auth>>(
      `${this.url}/loginStaff`,
      { email, password },
      {
        withCredentials: true,
        headers: new HttpHeaders({ "Content-Type": "application/json" }),
      }
    );
  }

  publishAuth(auth: Auth) {
    this.authSubject.next(auth);
  }

  getAuthObservable() {
    return this.authSubject.asObservable();
  }

  isLoggedIn(): boolean {

    const auth = localStorage.AUTH;

    if (auth) return true;
    else return false;
  }

  changeStoreId(storeId: number) {
    const auth = localStorage.AUTH;

    const authObj = JSON.parse(auth);

    localStorage.removeItem('AUTH');

    authObj.storeId = storeId;

    localStorage.setItem('AUTH', JSON.stringify(authObj));

    this.publishAuth(authObj);
  }

}
