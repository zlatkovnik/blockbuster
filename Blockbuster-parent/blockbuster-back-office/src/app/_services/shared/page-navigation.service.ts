import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { PageNav } from "../../_models/helper/page-nav.model";

@Injectable({
  providedIn: "root",
})
export class PageNavigationService {
  private pageNavigationSubject = new Subject<PageNav>();
  public pageNav;


  constructor() {
    this.pageNav = new PageNav();
    this.pageNav.count = 20;
    this.pageNav.page = 1;
  }

  getObservable() {
    return this.pageNavigationSubject.asObservable();
  }

  publishNextPage(pageNav: PageNav) {
    this.pageNav = pageNav;
    this.pageNavigationSubject.next(pageNav);
  }
}
