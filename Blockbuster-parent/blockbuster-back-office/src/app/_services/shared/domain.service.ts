import { Injectable } from "@angular/core";
import { AppConfig } from "../../app-config";

@Injectable({
  providedIn: "root",
})
export class DomainService {
  constructor() {}

  public getAddress() {
    return AppConfig.API_ENDPOINT;
    // return AppConfig.API_ENDPOINT_DEV;
  }
}
