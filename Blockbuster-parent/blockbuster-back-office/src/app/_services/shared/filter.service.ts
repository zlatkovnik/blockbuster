import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FilterService {
  private filterSubject = new Subject<{ type: string; filterValue: string }>();

  constructor() {}

  getObservable() {
    return this.filterSubject.asObservable();
  }

  publishFilter(type: string, filterValue: string) {
    this.filterSubject.next({ type, filterValue });
  }
}
