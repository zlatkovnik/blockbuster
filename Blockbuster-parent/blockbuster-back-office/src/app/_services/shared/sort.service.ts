import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Sort } from "src/app/_models/helper/sort.model";

@Injectable({
  providedIn: "root",
})
export class SortService {
  private sortSubject = new Subject<string>();

  constructor() {}

  getObservable() {
    return this.sortSubject.asObservable();
  }

  publishSort(sort: string) {
    this.sortSubject.next(sort);
  }
}
