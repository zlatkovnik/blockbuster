import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Subject } from "rxjs";

import * as SockJS from "sockjs-client";
import { RentalRequest } from "src/app/_models/film/rental-request.model";
import * as Stomp from "stompjs";

@Injectable({
  providedIn: "root",
})
export class NotificationService {
  socket;
  stompClient;

  private rentalRequestSubject = new Subject<RentalRequest>();

  constructor() {
    this.socket = new SockJS(
      //"http://localhost:8080/ws"
      // "http://localhost:8081/ws"
      "http://praktikanti.dualsoft.net:8080/blockbuster-auth/ws"
    );

    const stompClient = Stomp.over(this.socket);
    stompClient.connect({}, () => {
      this.stompClient = stompClient;
      const that = this;
      stompClient.subscribe("/user", function (messageOutput) {
        const rr = JSON.parse(messageOutput.body) as RentalRequest;
        that.publishRentalRequest(rr);
      });
    });
  }

  getRentalRequestObservable() {
    return this.rentalRequestSubject.asObservable();
  }

  publishRentalRequest(rentalRequest: RentalRequest) {
    this.rentalRequestSubject.next(rentalRequest);
  }

  // getNotifications() {
  //   this.toastr.success("You have a message", null, {
  //     positionClass: "toast-bottom-right",
  //   });
  // }
}
