import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Actor } from "../_models/actor/actor";

import { Response } from "../_models/template/response.model";
import { DomainService } from "./shared/domain.service";

import { FilmsForActor } from "../_models/filmsForActors/filmsForActors";
import { ActorMoviedb } from "../_models/actor/actor-moviedb.model";
@Injectable({
  providedIn: "root",
})
export class ActorService {
  status;
  errorMessage;

  private url = this.domain.getAddress() + "/actors";
  urlAdr = this.url + "/page" + "/";

  constructor(private http: HttpClient, private domain: DomainService) {}

  getActors(term: String, count: number, offset: number): Observable<Actor[]> {
    return this.http.get<Actor[]>(
      this.url + `/search?name=${term.trim()}&count=${count}&offset=${offset}`
    );
  }

  getActor(id: number): Observable<Actor> {
    return this.http.get<Actor>(this.url + `/${id}`);
  }

  getAllActors() {
    return this.http.get<Actor[]>(this.url);
  }

  getActorsFromMovieDB(moviedbId: number) {
    return this.http.get<{ cast: ActorMoviedb[] }>(
      `http://api.themoviedb.org/3/movie/${moviedbId}/credits?api_key=15d2ea6d0dc1d476efbca3eba2b9bbfb`
    );
  }

  addActorsAndRoles(moviedbId: number) {
    return this.http.get<Response<undefined>>(
      `${this.url}/addActorsAndRoles/${moviedbId}`
    );
  }

  getActorsForPage(numPage, numOfCust) {
    return this.http.get<Response<Actor[]>>(
      this.urlAdr + numPage + "/" + numOfCust
    );
  }

  getActorsBySearchForPage(numPage, numOfCust, searchText) {
    return this.http.get<Response<Actor[]>>(
      this.urlAdr + numPage + "/" + numOfCust + "/" + searchText
    );
  }

  getTotalNumOfActorBySearchText(searchText) {
    return this.http.get<Number>(this.url + "/totalNum/" + searchText);
  }

  getTotalNumOfActors() {
    return this.http.get<Number>(this.url + "/totalNum");
  }

  updateActor(a: FilmsForActor) {
    const params = new HttpParams();
    const headers = new HttpHeaders({});
    return this.http.put(this.url + "/upadateFilmsForActor", a, {
      headers: headers,
      params: params,
    });
  }

  deleteFilmForActor(filmId: number, actorID: number) {
    console.log(this.url + "/deleteFilmsForActor/" + filmId + "/" + actorID);
    return this.http.delete(
      this.url + "/deleteFilmsForActor/" + filmId + "/" + actorID
    );
  }
}
