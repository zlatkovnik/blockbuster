import { StoreForEdit } from "./../_models/store/store-for-edit";

import { DomainService } from "./shared/domain.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store } from "../_models/store/store.model";
import { FilmInventoryForStore } from "../_models/store/film-inventory-for-store.model";
import { Response } from "../_models/template/response.model";
import { tap } from "rxjs/operators";
import { Country } from "../_models/address/country";
import { City } from "../_models/address/city";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class StoresService {
  private storeUrl = this.domain.getAddress() + "/stores";

  private countryUrl = this.domain.getAddress() + "/countries";

  constructor(private http: HttpClient, private domain: DomainService) {}

  getStores() {
    return this.http.get<Store[]>(this.storeUrl);
  }

  getStore(storeId: number) {
    return this.http.get<Response<Store>>(`${this.storeUrl}/${storeId}`);
  }

  getStoresByCity(city: string) {
    return this.http.get<Store[]>(`${this.storeUrl}/byCity?city=${city}`);
  }

  getCities() {
    return this.http.get<string[]>(this.storeUrl + "/cities");
  }

  getCountries() {
    return this.http.get<Country[]>(this.countryUrl);
  }

  getCityForCountry(countryId: number) {
    return this.http.get<City[]>(`${this.countryUrl}/getCities/${countryId}`);
  }

  editStore(store: StoreForEdit) {
    return this.http.put<Response<StoreForEdit>>(
      `${this.storeUrl}/editStore`,
      store
    );
  }

  getFreeManagers() {
    return this.http.get(`${this.storeUrl}/managers`);
  }

  addNewStore(store): Observable<Response<null>> {
    return this.http.post<Response<null>>(
      `${this.storeUrl}/addNewStore`,
      store,
      {
        withCredentials: true,
        headers: new HttpHeaders({ "Content-Type": "application/json" }),
      }
    );
  }
}
