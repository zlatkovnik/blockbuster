import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Auth } from "../_models/auth.model";
import { Customer } from "../_models/customer/customer";

import { Response } from "../_models/template/response.model";
import { DomainService } from "./shared/domain.service";

@Injectable({
  providedIn: "root",
})
export class CustomerService {
  url = this.domain.getAddress() + "/customers";
  urlAdr = this.url + "/page" + "/";

  constructor(private http: HttpClient, private domain: DomainService) { }

  getCustomeForPage(numPage, numOfCust) {
    return this.http.get<Response<Customer[]>>(
      this.urlAdr + numPage + "/" + numOfCust
    );
  }

  getTotalNumOfcustomers() {
    return this.http.get<Number>(this.url + "/totalNum");
  }

  updateCustomer(cust: Customer) {
    const params = new HttpParams();
    const headers = new HttpHeaders({});
    console.log("setCustomer");
    return this.http.post(this.url + "/updateCustomerBackOffice", cust, {
      headers: headers,
      params: params,
    });
  }

  getCustomersBySearchTextForPage(numPage, numOfCust, searchText) {
    return this.http.get<Response<Customer[]>>(
      this.urlAdr + numPage + "/" + numOfCust + "/" + searchText
    );
  }

  getTotalNumOfCustomersBySearch(searchText) {
    return this.http.get<Number>(this.url + "/totalNum/" + searchText);
  }

  getCustomersWithRequests() {
    const auth: Auth = JSON.parse(localStorage.AUTH);
    return this.http.get<Response<{ customerId: number; username: string }[]>>(
      `http://praktikanti.dualsoft.net:8080/blockbuster-auth/rentals/customers?storeId=${auth.storeId}`
      // `${this.domain.getAddress()}/rentals/customers?storeId=${auth.storeId}`
    );
  }
}
