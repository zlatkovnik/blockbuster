import { Auth } from './../_models/auth.model';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Response } from "../_models/template/response.model";
import { Staff } from "./../_models/staff/staff-for-list";
import { DomainService } from "./shared/domain.service";

@Injectable({
  providedIn: "root",
})
export class StaffService {
  private url = this.domain.getAddress() + "/staff";
  constructor(private http: HttpClient, private domain: DomainService) {}

  getStaff() {
    return this.http.get<Response<Staff[]>>(this.url + "/list");
  }

  updateStaff(staffId: number, storeId: number) {
    return this.http.put(this.url + "/changeStoreId", JSON.stringify({staffId,storeId}),{
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
    })
  }
}
