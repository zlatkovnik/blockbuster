import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { tap } from "rxjs/operators";
import { Category } from "src/app/_models/film/category.model";
import { Response } from "src/app/_models/template/response.model";
import { DomainService } from "../shared/domain.service";

@Injectable({
  providedIn: "root",
})
export class CategoryService {
  private url = this.domain.getAddress() + "/category";
  constructor(private http: HttpClient, private domain: DomainService) {}

  getAllCategories() {
    return this.http.get<Response<Category[]>>(`${this.url}/all`);
  }
}
