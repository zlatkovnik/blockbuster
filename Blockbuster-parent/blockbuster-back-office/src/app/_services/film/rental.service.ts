import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Auth } from "src/app/_models/auth.model";
import { RentalRequest } from "src/app/_models/film/rental-request.model";
import { Rental } from "src/app/_models/film/rental.model";
import { Response } from "src/app/_models/template/response.model";
import { DomainService } from "../shared/domain.service";

@Injectable({
  providedIn: "root",
})
export class RentalService {
  // private url = this.domain.getAddress() + "/rentals";
  // private url = "http://praktikanti.dualsoft.net:8080/blockbuster-auth/rentals";
  private url = "http://praktikanti.dualsoft.net:8080/blockbuster-auth/rentals";
  constructor(private http: HttpClient, private domain: DomainService) { }

  getRentalRequests(
    status: string = "all",
    username: string = "all",
    page: number = 1,
    count: number = 20
  ) {
    //storeId, customerId, status
    const auth: Auth = JSON.parse(localStorage.AUTH);
    return this.http.get<Response<RentalRequest[]>>(
      `${this.url}/requests?storeId=${auth.storeId}&username=${username}&status=${status}&page=${page}&count=${count}`
    );
  }

  getRentals(username: string) {
    const auth: Auth = JSON.parse(localStorage.AUTH);
    return this.http.get<Response<Rental[]>>(
      `${this.url}?storeId=${auth.storeId}&username=${username}`
    );
  }

  setStatus(rentalRequestId: number, status: "accepted" | "rejected") {
    return this.http.get<Response<RentalRequest[]>>(
      `${this.url}/setStatus?rentalRequestId=${rentalRequestId}&status=${status}`
    );
  }

  returnRental(rentalId: number) {
    return this.http.get<Response<undefined>>(`${this.url}/return/${rentalId}`);
  }
}
