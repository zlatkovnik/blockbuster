import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Inventory } from "src/app/_models/film/inventory.model";
import { FilmInventoryForStore } from "src/app/_models/store/film-inventory-for-store.model";
import { Response } from "src/app/_models/template/response.model";
import { DomainService } from "../shared/domain.service";

@Injectable({
  providedIn: "root",
})
export class InventoryService {
  private url = this.domain.getAddress() + "/inventory";
  constructor(private http: HttpClient, private domain: DomainService) {}

  addInventory(filmId: number, storeId: number) {
    return this.http.get<Response<Inventory>>(
      `${this.url}/add?filmId=${filmId}&storeId=${storeId}`
    );
  }

  getFilmInventoryPerStore(filmId: number) {
    return this.http.get<Response<FilmInventoryForStore[]>>(
      `${this.url}/filmInventoryPerStore?filmId=${filmId}`
    );
  }
}
