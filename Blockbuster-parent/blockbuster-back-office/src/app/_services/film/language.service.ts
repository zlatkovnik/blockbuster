import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Language } from "src/app/_models/film/language.model";
import { Response } from "src/app/_models/template/response.model";
import { DomainService } from "../shared/domain.service";

@Injectable({
  providedIn: "root",
})
export class LanguageService {
  private url = this.domain.getAddress() + "/languages";
  constructor(private http: HttpClient, private domain: DomainService) {}

  getAllLanguages() {
    return this.http.get<Response<Language[]>>(`${this.url}/all`);
  }
}
