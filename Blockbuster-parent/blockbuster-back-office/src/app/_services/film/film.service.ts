import { Auth } from './../../_models/auth.model';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FilmsForList } from "../../_models/film/films-for-list.model";
import { Response } from "../../_models/template/response.model";
import { PageNav } from "../../_models/helper/page-nav.model";
import { DomainService } from "../shared/domain.service";
import { FilmDetails } from "src/app/_models/film/film-details.model";
import { FilmForEdit } from "src/app/_models/film/film-for-edit.model";
import { FilmForList } from "src/app/_models/film/film-for-list.model";
import { FilmForAdd } from "src/app/_models/film/film-for-add.model";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FilmService {
  private url = this.domain.getAddress() + "/films";
  constructor(private http: HttpClient, private domain: DomainService) { }

  getFilmsForListPage(pageNav: PageNav, sort: string, filter: string) {
    const { page, count } = pageNav;
    return this.http.get<Response<FilmsForList>>(
      `${this.url}/filmsForListPage?page=${page}&count=${count}&sort=${sort}&filter=${filter}`
    );
  }

  getFilmDetails(filmId: number) {
    const auth : Auth = JSON.parse(localStorage.AUTH);
    return this.http.get<Response<FilmDetails>>(
      `${this.url}/details?filmId=${filmId}&storeId=${auth.storeId}`
    );
  }

  getFilmForEdit(filmId: number) {
    const auth : Auth = JSON.parse(localStorage.AUTH);
    return this.http.get<Response<FilmForEdit>>(
      `${this.url}/getFilmForEdit?filmId=${filmId}&storeId=${auth.storeId}`
    );
  }

  editFilm(film: FilmForEdit) {
    return this.http.put<Response<FilmForEdit>>(`${this.url}/edit`, film);
  }

  getFilmSuggestion(searchTerm: string) {
    return this.http.get<Response<FilmForList>>(
      `${this.url}/getAddSuggestion?searchTerm=${searchTerm}`
    );
  }

  addFilm(film: FilmForAdd): Observable<Response<FilmForAdd>> {
    return this.http.post<Response<FilmForAdd>>(`${this.url}/add`, film, {
      withCredentials: true,
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
    });
  }
  getFilmsForActor(id: number) {
    return this.http.get<FilmForList[]>(this.url + `/actor?id=${id}`);
  }
}
