import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FilmForList } from "../_models/film/film-for-list.model";
import { FilmsForList } from "../_models/film/films-for-list.model";
import { Response } from "../_models/template/response.model";

import { tap } from "rxjs/operators";
import { PageNav } from "../_models/helper/page-nav.model";
import { Sort } from "../_models/helper/sort.model";
import { DomainService } from "./shared/domain.service";

@Injectable({
  providedIn: "root",
})
export class FilmService {
  private url = this.domain.getAddress() + "/films";
  constructor(private http: HttpClient, private domain: DomainService) { }

  getFilmsForListPage(pageNav: PageNav, sort: Sort) {
    const { page, count } = pageNav;
    return this.http.get<Response<FilmsForList>>(
      `${this.url}/filmsForListPage?page=${page}&count=${count}&sort=${sort.value}`
    );
  }
  getFilmsForActor(id: number) {
    return this.http.get<FilmForList[]>(this.url + `/actor?id=${id}`);
  }
  getFilmsSuggestionForActor(id: number) {
    return this.http.get<FilmForList[]>(this.url + '/actor' + '/' + id);
  }

}
